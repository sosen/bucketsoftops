#include <stdio.h>
#include <string>
#include <vector>
#include "TLorentzVector.h"
#include <map>
#include <limits>

//#include "PathResolver/PathResolver.h"
using namespace std;

//put top bucket algo stuff inside bucketAlgo namespace
namespace bucketAlgo
{

  std::vector <TLorentzVector> compvec(std::vector <TLorentzVector> EVT, std::vector <TLorentzVector> set);

  //power set generator
  std::vector <std::vector <int> > pSet(std::vector <int> set, int offset=0);
  
  //complementary set generator
  std::vector <int> cSet(std::vector <int> set, std::vector <int> subset);

  // reconstructing neutrino
  std::vector <TLorentzVector> RecoPzNeutrino(TLorentzVector lepton, double METx, double METy);

  //class for bucket object
  class bucket
  {
  private:
    double Mbucket, pTbucket, etabucket;
    double mpairnum, ratio_min;
    double delW, delWLep;
    double normW, normT;
  public:
    std::vector <TLorentzVector> members;
    std::vector <TLorentzVector> nonBJETS;
    TLorentzVector nu;
    TLorentzVector BJET;
    double twB1delta;
    double twB2delta;
    int BIndex;
    std::vector <int> nonBmemberIndex;
    double dT, dTLep, dW, dWLep, MTW;

  ~bucket() {} //destructor

  bucket(); // default constructor

  bucket(std::vector <TLorentzVector> specnonbjets, std::vector <int> nonbindexset, std::vector <TLorentzVector> bjets, int bindex);

  bucket(TLorentzVector lepton, std::vector <TLorentzVector> nu_candidates, std::vector <TLorentzVector> bjets, int bindex);

  double getBucketMass();

  double getBucketPt();

  double getBucketEta();

  std::vector<int> getOrderlist();

   double DelTop(float norm=1);

   double DelTopLep(float norm=1);
 
   double DelW(float norm=1);

   double DelWLep(float norm=1);

  double WcandMnum();
  
  double WcandRatio();

  double twB1OptMetric();

  double twB2OptMetric(); 

  };

  struct bucketpairs
  {
    map< int , std::vector<bucketAlgo::bucket> > Bpairs;
    int solutionIndex;
    map< int , double > deltop; 
    map< int , double > delw;
    std::vector <int> Leftovers;
  };


  //function to get two top buckets (1 leptonic)
  bucketpairs doublebucket1Lep(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets, TLorentzVector lepton, double B1weight, double B1Wweight, double B2weight, double B2Wweight, double BWwt, double METx, double METy);

  //function to get two top buckets (all had)
  bucketpairs doublebucketAllhad(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets, double B1weight, double B1Wweight, double B2weight, double B2Wweight, double BWwt);

//namespace ends
}
