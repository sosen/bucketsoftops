#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string>
//#include "BucketofTops/bucket_all.h"
#include "BucketofTops/BucketofTops.h"
using namespace std;

//one leptonic bucket + one hadronic bucket version
BucketofTops::BucketofTops(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets, bool had, TLorentzVector lepton, double METx, double METy){

  if (had == false){
    //need a setter function for it
    ////smearwidth = 0.8
    //double delTopvariance  = 9.109;
    //double delTopLepvariance  = 27.48;
    //double delWLepvariance = 22;
    //double delWvariance    = 0.03289;
  
    ////smearwidth = 1.0
    //double delTopvariance     = 11.14;
    //double delTopLepvariance  = 27.29;
    //double delWLepvariance    = 51.21;
    //double delWvariance       = 0.03624;
    double delTopvariance     = 11.15;
    double delTopLepvariance  = 131.90;
    double delWvariance       = 0.03621;
    double delWLepvariance    = 0.01264;
  
    ////smearwidth = 1.2
    //double delTopvariance  = 13.23;
    //double delTopLepvariance  = 27.5;
    //double delWLepvariance = 51.27;
    //double delWvariance    = 0.04011;
  
  
    double firstP1Bucketwt1 = 1/delTopvariance;
    double firstP1Bucketwt2 = 1/delTopLepvariance;

  
    double firstP1WBucketwt1 = 1/delWvariance;
    double firstP1WBucketwt2 = 1/delWLepvariance;
  
    //double BWP1wt = 0;
    //double BWP1wt = 0.25;
    //double BWP1wt = 0.5;
    //double BWP1wt = 0.75;
    //double BWP1wt = 1;
    //double BWP1wt = 1.2;
    //double BWP1wt = 1.4;
    //double BWP1wt = 1.6;
    //double BWP1wt = 1.8;
    double BWP1wt = 2;
    //double BWP1wt = 2.2;
    //double BWP1wt = 2.4;
    //double BWP1wt = 2.6;
    //double BWP1wt = 3;
    //double BWP1wt = 3.2;
    //double BWP1wt = 3.4;
    //double BWP1wt = 3.6;
    //double BWP1wt = 3.8;
    //double BWP1wt = 4;
    //double BWP1wt = 4.2;
    //double BWP1wt = 4.4;
    //double BWP1wt = 4.6;
    //double BWP1wt = 4.8;
    //double BWP1wt = 5;
    //double BWP1wt = 8;
    //double BWP1wt = 16; 
  
    nonbinitcount = specnonbjets.size();
  
    allbucketpairstw = bucketAlgo::doublebucket1Lep(specbjets, specnonbjets, lepton, firstP1Bucketwt1, firstP1WBucketwt1, firstP1Bucketwt2, firstP1WBucketwt2, BWP1wt, METx, METy);
  
    Blist = allbucketpairstw.Bpairs[allbucketpairstw.solutionIndex];
    SetBucketVars(Blist);
  }


  if (had == true){
    //need a setter function for it
    ////smearwidth = 0.8
    //double delTopvariance1 = 9.121;
    //double delTopvariance2 = 9.096;
    //double delWvariance1   = 0.03286;
    //double delWvariance2   = 0.03292;
    //double delTopvariance  = 9.109;
    //double delWvariance    = 0.03289;
  
    ////smearwidth = 1.0
    //double delTopvariance1 = 11.15;
    //double delTopvariance2 = 11.13;
    //double delWvariance1   = 0.03621;
    //double delWvariance2   = 0.03627;
    //double delTopvariance  = 11.14;
    //double delWvariance    = 0.03624;
    double delTopvarianceHad = 11.15;
    double delWvarianceHad   = 0.03621;
    double delTopvarianceLep = 131.90;
    double delWvarianceLep   = 0.1264;
  
    ////smearwidth = 1.2
    //double delTopvariance1 = 13.23;
    //double delTopvariance2 = 13.21;
    //double delWvariance1   = 0.04007;
    //double delWvariance2   = 0.04015;
    //double delTopvariance  = 13.22;
    //double delWvariance    = 0.04011;
  
    ////RMS finder
    //double delTopvariance1 = 1;
    //double delTopvariance2 = 1;
    //double delWvariance1   = 1;
    //double delWvariance2   = 1;
    //double delTopvariance  = 1;
    //double delWvariance    = 1;
  
    double firstP1Bucketwt1 = 1/delTopvarianceHad;
    double firstP1Bucketwt2 = 1/delTopvarianceLep;
  
    double firstP1WBucketwt1 = 1/delWvarianceHad;
    double firstP1WBucketwt2 = 1/delWvarianceLep;
  
    //double BWP1wt = 0;
    //double BWP1wt = 0.25;
    //double BWP1wt = 0.5;
    double BWP1wt = 1;
    //double BWP1wt = 1.2;
    //double BWP1wt = 1.4;
    //double BWP1wt = 1.6;
    //double BWP1wt = 1.8;
    //double BWP1wt = 2;
    //double BWP1wt = 2.2;
    //double BWP1wt = 2.4;
    //double BWP1wt = 2.6;
    //double BWP1wt = 4;
    //double BWP1wt = 8;
    //double BWP1wt = 16;
   
    nonbinitcount = specnonbjets.size();
  
    allbucketpairstw = bucketAlgo::doublebucketAllhad(specbjets, specnonbjets, firstP1Bucketwt1, firstP1WBucketwt1, firstP1Bucketwt2, firstP1WBucketwt2, BWP1wt);
  
    Blist = allbucketpairstw.Bpairs[allbucketpairstw.solutionIndex];
    SetBucketVars(Blist);
  }
}


void BucketofTops::SetBucketVars(std::vector<bucketAlgo::bucket> Blist) {
  B1bjet = Blist[0].BJET;
  B2bjet = Blist[1].BJET;
  B1nonbjets = Blist[0].nonBJETS;
  B2nonbjets = Blist[1].nonBJETS;

  nonb1count = Blist[0].nonBJETS.size();
  nonb2count = Blist[1].nonBJETS.size();
  nonbISRcount = nonbinitcount - (nonb1count + nonb2count);

  for (int i = 0; i < Blist.size(); ++i) {
    if (Blist[i].getBucketMass() > -1) {
      mWcand.push_back(Blist[i].WcandMnum());
      mBucketPrim.push_back(Blist[i].getBucketMass());
      mratio.push_back(Blist[i].WcandRatio());
    }
  }
  
}

BucketofTops::~BucketofTops(){}
