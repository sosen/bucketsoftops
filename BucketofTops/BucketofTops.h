
#ifndef BucketofTops_HH
#define BucketofTops_HH

#include <stdio.h>
#include <math.h>
#include <string>
//#include "TTHbbObjects/Event.h"
#include "BucketofTops/bucket_all.h"
//#include "PathResolver/PathResolver.h"

class BucketofTops {
  
  public:
    bucketAlgo::bucketpairs allbucketpairstw; // includes the intermediates too
    std::vector<bucketAlgo::bucket> Blist; //
    std::vector<float> mWcand, mBucketPrim, mratio; //GeV
    int nonbinitcount, nonb1count, nonb2count, nonbISRcount; //per event
    TLorentzVector B1bjet, B2bjet; //per event
    vector<TLorentzVector> B1nonbjets, B2nonbjets; //per event

    BucketofTops(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets, bool had=true, TLorentzVector lepton = TLorentzVector(), double METx=0, double METy=0);
    //BucketofTops(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets);
    ~BucketofTops();
    void SetBucketVars(std::vector<bucketAlgo::bucket> Blist);

    //
  private:

};

#endif
