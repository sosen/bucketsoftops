# Instructions to run BucketsofTops

* First include and forward declare BucketsofTops:
```
#include "BucketofTops/BucketofTops.h"
```
```
class BucketofTops; //forward declare
```


## Input
* To run BucketsofTops in all-hadronic ttbar mode:
```
bool hadmode = true;
BucketofTops *m_buckets = new BucketofTops(specbjets, specnonbjets, hadmode);
```

* To run BucketsofTops in semi-leptonic ttbar mode:
```
bool hadmode = false;
BucketofTops *m_buckets = new BucketofTops(specbjets, specnonbjets, hadmode, lepton, METx, METy);
```

## Output

* To access the solution buckets:
```
bucketAlgo::bucketpairs allprebpairs = m_buckets->allbucketpairstw;

std::map< int , std::vector<bucketAlgo::bucket> > prebpairMap = allprebpairs.Bpairs;

int AlgSol = allprebpairs.solutionIndex;

bucketAlgo::bucket B1 = prebpairMap[AlgSol][0]; //first bucket 

bucketAlgo::bucket B2 = prebpairMap[AlgSol][1]; //second bucket 
```

* To access the indices of the non b-jets for the first bucket:
```
    std::vector <int> B1nonbIndex = B1.nonBmemberIndex;
```
(just replace B1 with B2 to access the indices of the non b-jets in the second bucket)
* The second bucket in semi-leptonic version only has a b-jet and a lepton, so non b-index not useful.

* To access the leftover jets after running buckets:
```
std::vector <int> l = allprebpairs.Leftovers;
```

* To access the index of the b-jet of a bucket (out of 0(first b-jet) and 1(second b-jet)):
```
int b = B1.BIndex;  //same for B2
```

* To access the deltaTop metric of the two buckets:
```
double dtop = allprebpairs.deltop[AlgSol];
```

* To access the deltaW metric of the two buckets:
```
double dw = allprebpairs.delw[AlgSol];
```

