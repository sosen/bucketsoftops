// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME G__dictionary

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *Jet_Dictionary();
   static void Jet_TClassManip(TClass*);
   static void *new_Jet(void *p = 0);
   static void *newArray_Jet(Long_t size, void *p);
   static void delete_Jet(void *p);
   static void deleteArray_Jet(void *p);
   static void destruct_Jet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Jet*)
   {
      ::Jet *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Jet));
      static ::ROOT::TGenericClassInfo 
         instance("Jet", "AnalysisObjects.h", 11,
                  typeid(::Jet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &Jet_Dictionary, isa_proxy, 0,
                  sizeof(::Jet) );
      instance.SetNew(&new_Jet);
      instance.SetNewArray(&newArray_Jet);
      instance.SetDelete(&delete_Jet);
      instance.SetDeleteArray(&deleteArray_Jet);
      instance.SetDestructor(&destruct_Jet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Jet*)
   {
      return GenerateInitInstanceLocal((::Jet*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Jet*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Jet_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Jet*)0x0)->GetClass();
      Jet_TClassManip(theClass);
   return theClass;
   }

   static void Jet_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RC_Jet_Dictionary();
   static void RC_Jet_TClassManip(TClass*);
   static void *new_RC_Jet(void *p = 0);
   static void *newArray_RC_Jet(Long_t size, void *p);
   static void delete_RC_Jet(void *p);
   static void deleteArray_RC_Jet(void *p);
   static void destruct_RC_Jet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RC_Jet*)
   {
      ::RC_Jet *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RC_Jet));
      static ::ROOT::TGenericClassInfo 
         instance("RC_Jet", "AnalysisObjects.h", 36,
                  typeid(::RC_Jet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &RC_Jet_Dictionary, isa_proxy, 0,
                  sizeof(::RC_Jet) );
      instance.SetNew(&new_RC_Jet);
      instance.SetNewArray(&newArray_RC_Jet);
      instance.SetDelete(&delete_RC_Jet);
      instance.SetDeleteArray(&deleteArray_RC_Jet);
      instance.SetDestructor(&destruct_RC_Jet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RC_Jet*)
   {
      return GenerateInitInstanceLocal((::RC_Jet*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RC_Jet*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RC_Jet_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RC_Jet*)0x0)->GetClass();
      RC_Jet_TClassManip(theClass);
   return theClass;
   }

   static void RC_Jet_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Lepton_Dictionary();
   static void Lepton_TClassManip(TClass*);
   static void *new_Lepton(void *p = 0);
   static void *newArray_Lepton(Long_t size, void *p);
   static void delete_Lepton(void *p);
   static void deleteArray_Lepton(void *p);
   static void destruct_Lepton(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Lepton*)
   {
      ::Lepton *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Lepton));
      static ::ROOT::TGenericClassInfo 
         instance("Lepton", "AnalysisObjects.h", 56,
                  typeid(::Lepton), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &Lepton_Dictionary, isa_proxy, 0,
                  sizeof(::Lepton) );
      instance.SetNew(&new_Lepton);
      instance.SetNewArray(&newArray_Lepton);
      instance.SetDelete(&delete_Lepton);
      instance.SetDeleteArray(&deleteArray_Lepton);
      instance.SetDestructor(&destruct_Lepton);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Lepton*)
   {
      return GenerateInitInstanceLocal((::Lepton*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Lepton*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Lepton_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Lepton*)0x0)->GetClass();
      Lepton_TClassManip(theClass);
   return theClass;
   }

   static void Lepton_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *reconstructed_event_Dictionary();
   static void reconstructed_event_TClassManip(TClass*);
   static void *new_reconstructed_event(void *p = 0);
   static void *newArray_reconstructed_event(Long_t size, void *p);
   static void delete_reconstructed_event(void *p);
   static void deleteArray_reconstructed_event(void *p);
   static void destruct_reconstructed_event(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::reconstructed_event*)
   {
      ::reconstructed_event *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::reconstructed_event));
      static ::ROOT::TGenericClassInfo 
         instance("reconstructed_event", "AnalysisObjects.h", 177,
                  typeid(::reconstructed_event), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &reconstructed_event_Dictionary, isa_proxy, 0,
                  sizeof(::reconstructed_event) );
      instance.SetNew(&new_reconstructed_event);
      instance.SetNewArray(&newArray_reconstructed_event);
      instance.SetDelete(&delete_reconstructed_event);
      instance.SetDeleteArray(&deleteArray_reconstructed_event);
      instance.SetDestructor(&destruct_reconstructed_event);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::reconstructed_event*)
   {
      return GenerateInitInstanceLocal((::reconstructed_event*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::reconstructed_event*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *reconstructed_event_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::reconstructed_event*)0x0)->GetClass();
      reconstructed_event_TClassManip(theClass);
   return theClass;
   }

   static void reconstructed_event_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_Jet(void *p) {
      return  p ? new(p) ::Jet : new ::Jet;
   }
   static void *newArray_Jet(Long_t nElements, void *p) {
      return p ? new(p) ::Jet[nElements] : new ::Jet[nElements];
   }
   // Wrapper around operator delete
   static void delete_Jet(void *p) {
      delete ((::Jet*)p);
   }
   static void deleteArray_Jet(void *p) {
      delete [] ((::Jet*)p);
   }
   static void destruct_Jet(void *p) {
      typedef ::Jet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Jet

namespace ROOT {
   // Wrappers around operator new
   static void *new_RC_Jet(void *p) {
      return  p ? new(p) ::RC_Jet : new ::RC_Jet;
   }
   static void *newArray_RC_Jet(Long_t nElements, void *p) {
      return p ? new(p) ::RC_Jet[nElements] : new ::RC_Jet[nElements];
   }
   // Wrapper around operator delete
   static void delete_RC_Jet(void *p) {
      delete ((::RC_Jet*)p);
   }
   static void deleteArray_RC_Jet(void *p) {
      delete [] ((::RC_Jet*)p);
   }
   static void destruct_RC_Jet(void *p) {
      typedef ::RC_Jet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RC_Jet

namespace ROOT {
   // Wrappers around operator new
   static void *new_Lepton(void *p) {
      return  p ? new(p) ::Lepton : new ::Lepton;
   }
   static void *newArray_Lepton(Long_t nElements, void *p) {
      return p ? new(p) ::Lepton[nElements] : new ::Lepton[nElements];
   }
   // Wrapper around operator delete
   static void delete_Lepton(void *p) {
      delete ((::Lepton*)p);
   }
   static void deleteArray_Lepton(void *p) {
      delete [] ((::Lepton*)p);
   }
   static void destruct_Lepton(void *p) {
      typedef ::Lepton current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Lepton

namespace ROOT {
   // Wrappers around operator new
   static void *new_reconstructed_event(void *p) {
      return  p ? new(p) ::reconstructed_event : new ::reconstructed_event;
   }
   static void *newArray_reconstructed_event(Long_t nElements, void *p) {
      return p ? new(p) ::reconstructed_event[nElements] : new ::reconstructed_event[nElements];
   }
   // Wrapper around operator delete
   static void delete_reconstructed_event(void *p) {
      delete ((::reconstructed_event*)p);
   }
   static void deleteArray_reconstructed_event(void *p) {
      delete [] ((::reconstructed_event*)p);
   }
   static void destruct_reconstructed_event(void *p) {
      typedef ::reconstructed_event current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::reconstructed_event

namespace {
  void TriggerDictionaryInitialization_libdictionary_Impl() {
    static const char* headers[] = {
"/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/extern/include",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/util",
"/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis",
"/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/../buckets",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.14.04-0d8dc/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/buckets/src/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libdictionary dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h")))  Jet;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h")))  RC_Jet;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h")))  Lepton;
struct __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h")))  reconstructed_event;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libdictionary dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "/afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/src/Analysis/AnalysisObjects.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Jet", payloadCode, "@",
"Lepton", payloadCode, "@",
"RC_Jet", payloadCode, "@",
"reconstructed_event", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libdictionary",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libdictionary_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libdictionary_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libdictionary() {
  TriggerDictionaryInitialization_libdictionary_Impl();
}
