# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.11

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/buckets

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/ccmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# Special rule for the target test
test:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running tests..."
	/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.11.0/Linux-x86_64/bin/ctest --force-new-ctest-process $(ARGS)
.PHONY : test

# Special rule for the target test
test/fast: test

.PHONY : test/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/buckets/CMakeFiles /afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/buckets/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /afs/cern.ch/work/s/sosen/ChongbinTop/CompleteAnalysis/bsm4tops_runscripts/histogramming/buckets/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named BucketofTops_lib

# Build rule for target.
BucketofTops_lib: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 BucketofTops_lib
.PHONY : BucketofTops_lib

# fast build rule for target.
BucketofTops_lib/fast:
	$(MAKE) -f buckets/CMakeFiles/BucketofTops_lib.dir/build.make buckets/CMakeFiles/BucketofTops_lib.dir/build
.PHONY : BucketofTops_lib/fast

#=============================================================================
# Target rules for targets named G__buckets_dictionary

# Build rule for target.
G__buckets_dictionary: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 G__buckets_dictionary
.PHONY : G__buckets_dictionary

# fast build rule for target.
G__buckets_dictionary/fast:
	$(MAKE) -f buckets/CMakeFiles/G__buckets_dictionary.dir/build.make buckets/CMakeFiles/G__buckets_dictionary.dir/build
.PHONY : G__buckets_dictionary/fast

#=============================================================================
# Target rules for targets named G__dictionary

# Build rule for target.
G__dictionary: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 G__dictionary
.PHONY : G__dictionary

# fast build rule for target.
G__dictionary/fast:
	$(MAKE) -f src/CMakeFiles/G__dictionary.dir/build.make src/CMakeFiles/G__dictionary.dir/build
.PHONY : G__dictionary/fast

#=============================================================================
# Target rules for targets named do_DataMCHists

# Build rule for target.
do_DataMCHists: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 do_DataMCHists
.PHONY : do_DataMCHists

# fast build rule for target.
do_DataMCHists/fast:
	$(MAKE) -f src/CMakeFiles/do_DataMCHists.dir/build.make src/CMakeFiles/do_DataMCHists.dir/build
.PHONY : do_DataMCHists/fast

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... rebuild_cache"
	@echo "... edit_cache"
	@echo "... test"
	@echo "... BucketofTops_lib"
	@echo "... G__buckets_dictionary"
	@echo "... G__dictionary"
	@echo "... do_DataMCHists"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

