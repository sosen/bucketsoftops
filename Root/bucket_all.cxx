#include <stdio.h>
#include <math.h>
#include <string>
#include <vector>
#include "TLorentzVector.h"
#include <map>
#include "TMath.h"
#include <limits>
#include "BucketofTops/bucket_all.h"

using namespace std;


//put top bucket algo stuff inside bucketAlgo namespace
namespace bucketAlgo
{

  double mTop = 173.5; //GeV
  double mW = 80.4; //GeV
  //double mTopIncomplete = 102.0; //GeV //this is for smearwidth=0.8*sqrt(E)  //to be replaced by median of leptonic top mass
  double mTopIncomplete = 101.7; //GeV //this is for smearwidth=1.0*sqrt(E)  //to be replaced by median of leptonic top mass
  //double mTopIncomplete = 101.3; //GeV //this is for smearwidth=1.2*sqrt(E)  //to be replaced by median of leptonic top mass

  std::vector <TLorentzVector> compvec(std::vector <TLorentzVector> EVT, std::vector <TLorentzVector> set) 
  {
    std::vector <TLorentzVector> compset;
    for (unsigned int i = 0; i < EVT.size(); ++i)
    {
      bool findflag = false;
      for (unsigned int j = 0; j < set.size(); ++j)
      {
        findflag = findflag || (EVT[i]==set[j]); 
      }
      if (!findflag) {compset.push_back(EVT[i]);}
    }
    return compset;
  };

  //power set generator
  std::vector <std::vector <int> > pSet(std::vector <int> set, int offset)
  {
    //power set size : 2^n
    unsigned int pow_set_size = pow(2, set.size());
    unsigned int counter;
    std::vector <std::vector <int> > pVec;
    //Run counter from 000..1 and stop before 111..1 (111..1 excluded)
    for(counter = 1; counter < pow_set_size-offset; counter++)
    {
      std::vector <int> v;
      for(unsigned int j = 0; j < set.size(); j++)
      {
        //if jth bit in the counter is 1 print the jth element
        if(counter & (1<<j))
        v.push_back(set[j]);
      }
      pVec.push_back(v); 
      v.clear();
    }
    return pVec;
  }
 
  
  //complementary set generator
  std::vector <int> cSet(std::vector <int> set, std::vector <int> subset)
  {
    std::vector <int> cVec;
    for (unsigned int i = 0; i < set.size(); ++i)
    {
      if (find(subset.begin(), subset.end(), set[i]) != subset.end())
      {
        continue;
      }
      cVec.push_back(set[i]);
    }
    return cVec;
  };
  
  // reconstructing neutrino
  std::vector <TLorentzVector> RecoPzNeutrino(TLorentzVector lepton, double METx, double METy) 
  {
    double A, D, mu, pTnusquare;
    mu = (0.5*mW*mW) + (lepton.Px()*METx + lepton.Py()*METy);
    pTnusquare = (METx*METx) + (METy*METy);

    D = (pow(mu,2)*pow(lepton.Pz(),2)/pow(lepton.Pt(),4)) - ((pow(lepton.E(),2)*pTnusquare - pow(mu,2))/pow(lepton.Pt(),2));

    D = (D < 0) ? 0 : D; // revisit this

    A = mu*(lepton.Pz()/pow(lepton.Pt(),2));
     
    std::vector <TLorentzVector> nu_cand; //neutrino candidates
    if (D == 0) {
      double pznu = A;
      TLorentzVector nu;
      nu.SetPxPyPzE(METx, METy, pznu, sqrt(METx*METx + METy*METy + pznu*pznu));
      nu_cand.push_back(nu);
    }
    else { // D > 0
      double pznu1 = A + sqrt(D);
      double pznu2 = A - sqrt(D);
      TLorentzVector nu1;
      nu1.SetPxPyPzE(METx, METy, pznu1, sqrt(METx*METx + METy*METy + pznu1*pznu1));
      nu_cand.push_back(nu1);
      TLorentzVector nu2;
      nu2.SetPxPyPzE(METx, METy, pznu2, sqrt(METx*METx + METy*METy + pznu2*pznu2));
      nu_cand.push_back(nu2);
    }
    
    return nu_cand;

  }

  //buckets
  bucket::bucket()
  {
    Mbucket = -9999; //GeV
    pTbucket = -9999; //GeV
    etabucket = -9999;
    mpairnum = -9999; //GeV
    twB1delta = -9999; //GeV^2
    twB2delta = -9999; //GeV^2
    normW = 1;
    normT = 1;
    dT = -9999;
    dTLep = -9999;
    dW = -9999;
    dWLep = -9999;
    MTW = -9999; //GeV
  }

  //for hadronic bucket
  bucket::bucket(std::vector <TLorentzVector> specnonbjets, std::vector <int> nonbindexset, std::vector <TLorentzVector> bjets, int bindex)
  {
    BIndex = bindex;
    nonBmemberIndex = nonbindexset;
    //std::vector <TLorentzVector> nonbA;
    for (unsigned int k = 0; k < nonBmemberIndex.size(); ++k)
    {
      nonBJETS.push_back(specnonbjets[nonBmemberIndex[k]]);
    }
    BJET = bjets[BIndex];
    members.push_back(BJET);
    TLorentzVector b;
    b = b + BJET;
    for(unsigned int i =0; i < nonBJETS.size(); ++i)
    {
      b = b + nonBJETS[i];
      members.push_back(nonBJETS[i]);
    }
    Mbucket = b.M();
    pTbucket = b.Pt();
    etabucket = b.Eta();
  } 

  //for leptonic bucket
  bucket::bucket(TLorentzVector lepton, std::vector <TLorentzVector> nu_candidates, std::vector <TLorentzVector> bjets, int bindex)
  {
    BIndex = bindex;
    nonBmemberIndex = { 9999 }; // for lepton
    nonBJETS.push_back(lepton);
    BJET = bjets[BIndex];
    members.push_back(BJET);
    TLorentzVector b;
    b = b + BJET;
    for(unsigned int i =0; i < nonBJETS.size(); ++i)
    {
      b = b + nonBJETS[i];
      members.push_back(nonBJETS[i]);
    }
    // adding the neutrino
    int nuindex = 999;
    double residualM = std::numeric_limits<float>::infinity();

    for (unsigned int i =0; i < nu_candidates.size(); ++i)
    {
      TLorentzVector testb;
      testb = b + nu_candidates[i];
      double testresidualM = abs(testb.M() - mTop);
      if (residualM > testresidualM)
      {
        residualM = testresidualM;
	nuindex = i;
      }
    }
    nu = nu_candidates[nuindex];

    Mbucket = (b + nu).M();
    pTbucket = (b + nu).Pt();
    etabucket = (b + nu).Eta();
  } 
  double bucket::getBucketMass() {return Mbucket;}

  double bucket::getBucketPt() {return pTbucket;}

  double bucket::getBucketEta() {return etabucket;}

  std::vector<int> bucket::getOrderlist()
  {
    std::vector<int> orderlist;
    for (unsigned int i=0; i < members.size(); ++i)
    {
      orderlist.push_back(i+1);
    }
    return orderlist;
  }

 
  double bucket::DelTop(float norm)
  {
    normT = norm;
    dT = normT*(Mbucket - mTop);
    return dT;
  }
 
  double bucket::DelTopLep(float norm)
  {
    normT = norm;
    dTLep = normT*(Mbucket - mTop);
    return dTLep;
  }
 
  double bucket::DelW(float norm)
  {
    normW = norm;
    ratio_min = pow(10, 10); 
    delW = pow(10,10);
    int sizenonB = nonBJETS.size();
       
    for (int i = 0; i < sizenonB; ++i)
    {
      for (int j = i; j < sizenonB; ++j)
      {
        TLorentzVector temp;
        if (j == i) {temp = nonBJETS[i];} //in case of a merged W candidate
        else {temp = nonBJETS[i]+nonBJETS[j];}
        double ddsigned = (temp.M()/Mbucket) - (mW/mTop);
        double dd = abs(ddsigned);
        if (ratio_min > dd) //
        {
          delW = ddsigned;
          ratio_min = dd;
          mpairnum = temp.M();
        }
      }
    }
    dW = normW*delW;
    return dW;
  }

  double bucket::DelWLep(float norm)
  {
    normW = norm;
    double delWLep = ((nonBJETS[0] + nu).M()/Mbucket) - (mW/mTop);
    dWLep = normW*delWLep;
    return dWLep;
  }

  double bucket::WcandMnum() {return mpairnum;}
  
  double bucket::WcandRatio() {return ratio_min;}

  double bucket::twB1OptMetric() {return (Mbucket - mTop)*(Mbucket - mTop);} 

  double bucket::twB2OptMetric() {return (Mbucket - mTopIncomplete)*(Mbucket - mTopIncomplete);} 

  //function to get two top buckets (1 leptonic bucket)
  bucketpairs doublebucket1Lep(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets, TLorentzVector lepton, double B1weight, double B1Wweight, double B2weight, double B2Wweight, double BWwt, double METx, double METy)
  {
    std::vector <bucketAlgo::bucket> Blist; //B1 and B2
    bucketAlgo::bucket B1, B2;
    int solIndex;
    int Index = 0;
    bucketpairs allbpairs;
    int nonbjetsize = specnonbjets.size();
    std::vector <std::vector <int> > nonbindexset1;
    std::vector <int> nonbset;
    for (int j = 0; j < nonbjetsize; ++j) {nonbset.push_back(j);}
    nonbindexset1 = pSet(nonbset); //no offset
    double Deltatw = pow(10,10); //arbit large number

    // reconstruct neutrino pz
   
    std::vector<TLorentzVector> nu_candidates; //neutrino candidates
    nu_candidates = RecoPzNeutrino(lepton, METx, METy);

    for (unsigned int i = 0; i < nonbindexset1.size(); ++i) //looping over all possible buckets
    {
      for (unsigned int i1 = 0; i1 < specbjets.size(); ++i1)
      {
        bucketAlgo::bucket b1(specnonbjets, nonbindexset1[i], specbjets, 1-i1);
	bucketAlgo::bucket b2(lepton, nu_candidates, specbjets, i1);

        double b1Distance = pow(b1.DelTop(B1weight),2);
	//double b2Distance = pow(b2.DelTopLep(B2weight),2);
	double b2Distance = pow(b2.DelTopLep(B2weight),2);
        double b1WDistance = pow(b1.DelW(B1Wweight),2);
        double b2WDistance = pow(b2.DelWLep(B2Wweight),2);
	////DelWlep is useless, need to be replaced by a more sensible metric, currently just a constant so being commented out
	//double b2WDistance = pow(b2.DelWLep(B2Wweight, METx, METy),2);

	allbpairs.deltop[Index] = b1Distance + b2Distance;
	allbpairs.delw[Index] = b1WDistance; //+ b2WDistance; //b2WDistance being switched off

        double del;

	//since B1wt = 1 as optimum is established, explicit sorting is redundant and so removed here on
        allbpairs.Bpairs[Index].push_back(b1);
        allbpairs.Bpairs[Index].push_back(b2);
        //del = BWwt*(b1WDistance + b2WDistance) + (b1Distance + b2Distance); //b2WDistance being switched off
        del = BWwt*(b1WDistance) + (b1Distance + b2Distance);
        if (del < Deltatw)
        {
            Deltatw = del;
            B1 = b1;
            B2 = b2;
	    solIndex = Index;
        }
        Index++;	
      }
    } // loop over all possible buckets ends
    allbpairs.solutionIndex = solIndex;
    allbpairs.Leftovers = cSet(cSet(nonbset, B1.nonBmemberIndex), B2.nonBmemberIndex);
    Blist.push_back(B1);
    Blist.push_back(B2);
    //return Blist;
    return allbpairs;
  };


  //function to get two top buckets (all hadronic buckets)
  bucketpairs doublebucketAllhad(std::vector<TLorentzVector> specbjets, std::vector<TLorentzVector> specnonbjets, double B1weight, double B1Wweight, double B2weight, double B2Wweight, double BWwt)
  {
    std::vector <bucketAlgo::bucket> Blist; //B1 and B2
    bucketAlgo::bucket B1, B2;
    int solIndex;
    int Index = 0;
    bucketpairs allbpairs;
    int nonbjetsize = specnonbjets.size();
    std::vector <std::vector <int> > nonbindexset1;
    std::vector <int> nonbset;
    for (int j = 0; j < nonbjetsize; ++j) {nonbset.push_back(j);}
    nonbindexset1 = pSet(nonbset); //no offset
    double Deltatw = pow(10,10); //arbit large number
    for (int i = 0; i < nonbindexset1.size(); ++i) //looping over all possible buckets
    {
      std::vector <std::vector <int> > nonbindexset2;
      std::vector <int> restjetset = cSet(nonbset, nonbindexset1[i]);
      nonbindexset2 = pSet(restjetset); 
      for (int i1 = 0; i1 < nonbindexset2.size(); ++i1)
      {
        bucketAlgo::bucket b1(specnonbjets, nonbindexset1[i], specbjets, 0);
	bucketAlgo::bucket b2(specnonbjets, nonbindexset2[i1], specbjets, 1);

        double b1Distance = pow(b1.DelTop(B1weight),2);
	double b2Distance = pow(b2.DelTop(B2weight),2);
        double b1WDistance = pow(b1.DelW(B1Wweight),2);
	double b2WDistance = pow(b2.DelW(B2Wweight),2);

	allbpairs.deltop[Index] = b1Distance + b2Distance;
	allbpairs.delw[Index] = b1WDistance + b2WDistance;

        double del;

	//since B1wt = 1 as optimum is established, explicit sorting is redundant and so removed here on
        allbpairs.Bpairs[Index].push_back(b1);
        allbpairs.Bpairs[Index].push_back(b2);
        del = BWwt*(b1WDistance + b2WDistance) + (b1Distance + b2Distance);
        if (del < Deltatw)
        {
            Deltatw = del;
            B1 = b1;
            B2 = b2;
	    solIndex = Index;
        }
        Index++;	
      }
    } // loop over all possible buckets ends
    allbpairs.solutionIndex = solIndex;
    allbpairs.Leftovers = cSet(nonbset, B1.nonBmemberIndex);
    Blist.push_back(B1);
    Blist.push_back(B2);
    //return Blist;
    return allbpairs;
  };



//namespace ends
}
